import os
import subprocess
import time

import requests

my_host = "142.132.173.40"
token = "5121247126:AAF0QnlP4ncEBZay9TjyY0aK176ZlB82-eE"
url = "https://api.telegram.org/bot"
channel_id = "@check_flakevic_vpn"
url += token
method = url + "/sendMessage"


def send_telegram(text: str):
    r = requests.post(method, data={
        "chat_id": channel_id,
        "text": text
    })

    if r.status_code != 200:
        raise Exception("post_text error")


def ping(host):
    param = '-n'
    command = ['ping', param, '1', host]
    return subprocess.call(command) == 0


if __name__ == '__main__':
    send_telegram('--Start--')
    i = 0
    while True:
        r = ping(my_host)
        if not r:
            i = 0
            send_telegram("Something wrong with vpn")
        else:
            i += 1
            if i % 24 == 0:
                send_telegram('OK')
        time.sleep(3600)
